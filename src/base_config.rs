#[allow(unused)]
use {
    crate::meta::MetaTopDir,
    crate::recipe::Recipe,
    crate::utils::{scan_dir, scan_dir_for_dir, scan_dir_for_file, verify_content},
    anyhow::{Context, Error, Result},
    jlogger::{jdebug, jerror, jinfo, jwarn},
    log::{debug, error, info, warn},
    std::collections::HashMap,
    std::ffi::OsStr,
    std::fmt::Display,
    std::fs,
    std::path::{Path, PathBuf},
};

#[derive(Default)]
pub struct BaseConfig {
    conf: Option<String>,
    bblayer: Option<String>,
    dev_setup: Option<String>,
}

impl BaseConfig {
    pub fn new(meta_top: &str, build_dir: &str, base_config_dif: Option<String>) -> Result<Self> {
        let mut conf: Option<String> = None;
        let mut bblayer: Option<String> = None;
        let mut dev_setup: Option<String> = None;

        if let Some(base_dir) = base_config_dif {
            let p = Path::new(&base_dir);
            if !p.is_dir() {
                return Err(Error::msg("Invalid base config directory"));
            }
            scan_dir_for_file(p, &mut |f: PathBuf| -> Result<()> {
                let l = f.file_name().unwrap();
                if l == OsStr::new("local.conf.sample") {
                    let content = fs::read_to_string(&f)?;
                    if let Some(c) = verify_content(content) {
                        conf = Some(c);
                    }
                }

                if l == OsStr::new("bblayers.conf.sample") {
                    let content = fs::read_to_string(&f)?;
                    if let Some(c) = verify_content(content) {
                        let modified = c.replace("##OEROOT##", meta_top);
                        bblayer = Some(modified);
                    }
                }

                if l == OsStr::new("dev-init-build-env.sample") {
                    let content = fs::read_to_string(&f)?;
                    if let Some(c) = verify_content(content) {
                        let modified = c
                            .replace("##OEROOT##", meta_top)
                            .replace("##BUILDDIR##", build_dir);
                        dev_setup = Some(modified);
                    }
                }
                Ok(())
            })?
        } else {
            let content = std::include_str!("../base_config/local.conf.sample");
            if let Some(c) = verify_content(content.to_owned()) {
                conf = Some(c);
            }

            let content = std::include_str!("../base_config/bblayers.conf.sample");
            if let Some(c) = verify_content(content.to_owned()) {
                let modified = c.replace("##OEROOT##", meta_top);
                bblayer = Some(modified);
            }

            let content = std::include_str!("../base_config/dev-init-build-env.sample");
            if let Some(c) = verify_content(content.to_owned()) {
                let modified = c
                    .replace("##OEROOT##", meta_top)
                    .replace("##BUILDDIR##", build_dir);
                dev_setup = Some(modified);
            }
        }

        Ok(BaseConfig {
            conf,
            bblayer,
            dev_setup,
        })
    }

    pub fn conf(&self) -> Option<&str> {
        if let Some(c) = &self.conf {
            Some(c.as_str())
        } else {
            None
        }
    }

    pub fn bblayer(&self) -> Option<&str> {
        if let Some(c) = &self.bblayer {
            Some(c.as_str())
        } else {
            None
        }
    }

    pub fn dev_setup(&self) -> Option<&str> {
        if let Some(c) = &self.dev_setup {
            Some(c.as_str())
        } else {
            None
        }
    }
}
