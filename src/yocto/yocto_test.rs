#[cfg(test)]
use crate::yocto::{YoctoVar, YoctoVarName, YoctoVarType};
use jlogger::JloggerBuilder;
#[test]
fn yoctovar_01() {
    assert_eq!(YoctoVar::valid_varable_name("META"), true);
    assert_eq!(YoctoVar::valid_varable_name("M_ETA"), true);
    assert_eq!(YoctoVar::valid_varable_name("METa"), false);
    assert_eq!(YoctoVar::valid_varable_name("_META"), false);
    assert_eq!(YoctoVar::valid_varable_name("1META"), false);
    assert_eq!(YoctoVar::valid_varable_name("M1ETA"), true);
    assert_eq!(YoctoVar::valid_varable_name("M1E2T3A"), true);
    assert_eq!(YoctoVar::valid_varable_name("M1E_T_A"), true);
    assert_eq!(YoctoVar::valid_varable_name("M1_E_T_A"), true);
    assert_eq!(YoctoVar::valid_varable_name("abc"), false);
}

#[test]
fn yoctovar_02() {
    assert_eq!(YoctoVar::valid_override("append"), true);
    assert_eq!(YoctoVar::valid_override("remove"), true);
    assert_eq!(YoctoVar::valid_override("a-c"), true);
    assert_eq!(YoctoVar::valid_override("ab-"), true);
    assert_eq!(YoctoVar::valid_override("_ab"), false);
    assert_eq!(YoctoVar::valid_override("Abc"), false);
    assert_eq!(YoctoVar::valid_override("aBc"), false);
    assert_eq!(YoctoVar::valid_override("abC"), false);
    assert_eq!(YoctoVar::valid_override("a c"), false);
    assert_eq!(YoctoVar::valid_override("ab "), false);
    assert_eq!(YoctoVar::valid_override("9bc"), false);
    assert_eq!(YoctoVar::valid_override("a9c"), true);
    assert_eq!(YoctoVar::valid_override("ab9"), true);
    assert_eq!(YoctoVar::valid_override(""), false);
}

#[test]
fn yoctovar_03() {
    assert_eq!(YoctoVar::fix_str(" bc"), "bc");
    assert_eq!(YoctoVar::fix_str("a c"), "a c");
    assert_eq!(YoctoVar::fix_str("ab "), "ab");
}

#[test]
fn yoctovar_04() {
    assert!(YoctoVar::new(None).is_ok());
    assert!(YoctoVar::new(Some("abc")).is_err());
    assert!(YoctoVar::new(Some("AB C")).is_err());
    assert!(YoctoVar::new(Some("ABC ")).is_ok());
}

#[test]
fn yoctovar_05() {
    let mut yv = YoctoVar::new(None).unwrap();
    yv.update("ABC", "abc_value").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc_value"));

    yv.update("ABC_over", "over_value").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_value")
    );

    yv.update("ABC[flg]", "flg_value").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("flg_value")
    );

    yv.update("ABC_append", " appval").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc_value appval"));

    yv.update("ABC_append", "appval2").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(
        yv.get_value(YoctoVarType::Value),
        Some("abc_value appvalappval2")
    );

    yv.update("ABC_remove", "appval2").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(
        yv.get_value(YoctoVarType::Value),
        Some("abc_value appvalappval2")
    );

    yv.update("ABC_remove", "appvalappval2").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc_value"));

    yv.update("ABC_over_append", " appval").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc_value"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_value appval")
    );

    yv.update("ABC_over_remove", "appval").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc_value"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_value")
    );

    yv.update("ABC_append_over", " appval").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc_value"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("abc_value appval")
    );

    yv.update("ABC_remove_over", "abc_value").unwrap();
    assert_eq!(yv.name(), "ABC");
    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc_value"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("")
    );
}

#[test]
fn yoctovar_06() {
    match YoctoVar::parse_name("ABC") {
        YoctoVarName::Name(n) => {
            assert_eq!(n, String::from("ABC"));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC_append") {
        YoctoVarName::NameValAppend(n) => {
            assert_eq!(n, String::from("ABC"));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC_remove") {
        YoctoVarName::NameValRemove(n) => {
            assert_eq!(n, String::from("ABC"));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC[flg]") {
        YoctoVarName::NameFlag((n, f)) => {
            assert_eq!((n, f), (String::from("ABC"), String::from("flg")));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC_over") {
        YoctoVarName::NameOverride((n, o)) => {
            assert_eq!((n, o), (String::from("ABC"), String::from("over")));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC_over_append") {
        YoctoVarName::NameOverrideAppend((n, o)) => {
            assert_eq!((n, o), (String::from("ABC"), String::from("over")));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC_over_remove") {
        YoctoVarName::NameOverrideRemove((n, o)) => {
            assert_eq!((n, o), (String::from("ABC"), String::from("over")));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC_append_over") {
        YoctoVarName::NameAppendOverride((n, o)) => {
            assert_eq!((n, o), (String::from("ABC"), String::from("over")));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("ABC_remove_over") {
        YoctoVarName::NameRemoveOverride((n, o)) => {
            assert_eq!((n, o), (String::from("ABC"), String::from("over")));
        }
        _ => assert!(false),
    }

    match YoctoVar::parse_name("abc_remove_over") {
        YoctoVarName::InvalidName => {}
        _ => assert!(false),
    }
}

#[test]
fn yoctovar_07() {
    let mut yv = YoctoVar::new(None).unwrap();
    assert_eq!(yv.get_value(YoctoVarType::Value), Some(""));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        None
    );
    assert_eq!(yv.get_value(YoctoVarType::Flag(String::from("flg"))), None);

    yv.set_value(YoctoVarType::Value, "abc");
    yv.set_value(YoctoVarType::Override(String::from("over")), "abc_over");
    yv.set_value(YoctoVarType::Flag(String::from("flg")), "abc_flg");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("abc_over")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("abc_flg")
    );
}

#[test]
fn yoctovar_08() {
    use regex::Regex;

    let mut yv = YoctoVar::new(None).unwrap();
    yv.set_value(YoctoVarType::Flag(String::from("flag1")), "");
    yv.set_value(YoctoVarType::Flag(String::from("flag2")), "");
    yv.set_value(YoctoVarType::Flag(String::from("flag3")), "");
    yv.set_value(YoctoVarType::Flag(String::from("flag4")), "");
    yv.set_value(YoctoVarType::Flag(String::from("flag5")), "");

    let rg = Regex::new(r"flag[1-5]").unwrap();

    let mut it = yv.get_flags().into_iter();
    assert!(rg.is_match(it.next().unwrap()));
    assert!(rg.is_match(it.next().unwrap()));
    assert!(rg.is_match(it.next().unwrap()));
    assert!(rg.is_match(it.next().unwrap()));
    assert!(rg.is_match(it.next().unwrap()));
    assert_eq!(it.next(), None);
}

#[test]
fn yoctovar_09() {
    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.set_value(YoctoVarType::Value, "abc");
    yv.set_value(YoctoVarType::Override(String::from("over")), "over_abc");
    yv.set_value(YoctoVarType::Flag(String::from("flg")), "flg_abc");

    yv.append(YoctoVarType::Value, "appval");
    yv.append(YoctoVarType::Override(String::from("over")), "appval");
    yv.append(YoctoVarType::Flag(String::from("flg")), "appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abcappval"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_abcappval")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("flg_abcappval")
    );

    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.append(YoctoVarType::Value, "appval");
    yv.append(YoctoVarType::Override(String::from("over")), "over_appval");
    yv.append(YoctoVarType::Flag(String::from("flg")), "flg_appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("appval"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_appval")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("flg_appval")
    );
}

#[test]
fn yoctovar_10() {
    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.set_value(YoctoVarType::Value, "abc");
    yv.set_value(YoctoVarType::Override(String::from("over")), "over_abc");
    yv.set_value(YoctoVarType::Flag(String::from("flg")), "flg_abc");

    yv.prepend(YoctoVarType::Value, "appval");
    yv.prepend(YoctoVarType::Override(String::from("over")), "appval");
    yv.prepend(YoctoVarType::Flag(String::from("flg")), "appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("appvalabc"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("appvalover_abc")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("appvalflg_abc")
    );

    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.prepend(YoctoVarType::Value, "appval");
    yv.prepend(YoctoVarType::Override(String::from("over")), "over_appval");
    yv.prepend(YoctoVarType::Flag(String::from("flg")), "flg_appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("appval"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_appval")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("flg_appval")
    );
}

#[test]
fn yoctovar_11() {
    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.set_value(YoctoVarType::Value, "abc");
    yv.set_value(YoctoVarType::Override(String::from("over")), "over_abc");
    yv.set_value(YoctoVarType::Flag(String::from("flg")), "flg_abc");

    yv.add_after(YoctoVarType::Value, "appval");
    yv.add_after(YoctoVarType::Override(String::from("over")), "appval");
    yv.add_after(YoctoVarType::Flag(String::from("flg")), "appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("abc appval"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_abc appval")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("flg_abc appval")
    );

    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.add_after(YoctoVarType::Value, "appval");
    yv.add_after(YoctoVarType::Override(String::from("over")), "over_appval");
    yv.add_after(YoctoVarType::Flag(String::from("flg")), "flg_appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("appval"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_appval")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("flg_appval")
    );
}

#[test]
fn yoctovar_12() {
    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.set_value(YoctoVarType::Value, "abc");
    yv.set_value(YoctoVarType::Override(String::from("over")), "over_abc");
    yv.set_value(YoctoVarType::Flag(String::from("flg")), "flg_abc");

    yv.add_before(YoctoVarType::Value, "appval");
    yv.add_before(YoctoVarType::Override(String::from("over")), "appval");
    yv.add_before(YoctoVarType::Flag(String::from("flg")), "appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("appval abc"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("appval over_abc")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("appval flg_abc")
    );

    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.add_before(YoctoVarType::Value, "appval");
    yv.add_before(YoctoVarType::Override(String::from("over")), "over_appval");
    yv.add_before(YoctoVarType::Flag(String::from("flg")), "flg_appval");

    assert_eq!(yv.get_value(YoctoVarType::Value), Some("appval"));
    assert_eq!(
        yv.get_value(YoctoVarType::Override(String::from("over"))),
        Some("over_appval")
    );
    assert_eq!(
        yv.get_value(YoctoVarType::Flag(String::from("flg"))),
        Some("flg_appval")
    );
}

#[test]
fn yoctovar_13() {

    let value_str = "${ABC}/a/b/c, ${DEF}, a${GHI}";
    let v = YoctoVar::variables(value_str);
    assert_eq!(v.len(), 3);
    for i in v {
        match i.as_str() {
            "ABC" => {},
            "DEF" => {},
            "GHI" => {},
            _ => assert!(false),
        }
    }

    let value_str = "/a/b/c";
    let v = YoctoVar::variables(value_str);
    assert_eq!(v.len(), 0);
}

#[test]
fn yoctovar_14() {
    
    let mut yv = YoctoVar::new(Some("ABC")).unwrap();
    yv.set_default_value("default_abc");
    yv.set_value(YoctoVarType::Value, "abc");
    yv.set_value(YoctoVarType::Override(String::from("flag1")), "flag1_val");
    yv.set_value(YoctoVarType::Override(String::from("flag2")), "flag2_val");
    yv.set_value(YoctoVarType::Override(String::from("flag3")), "flag3_val");

    assert_eq!(yv.value(None), "abc");
    assert_eq!(yv.value(Some("flag1")), "flag1_val");
    assert_eq!(yv.value(Some("flag1:flag2")), "flag2_val");
    assert_eq!(yv.value(Some("flag1:flag2:flag3")), "flag3_val");
    yv.set_value(YoctoVarType::Value, "");
    assert_eq!(yv.value(None), "default_abc");

}

#[test]
fn yoctovar_15() {
    JloggerBuilder::new()
        .max_level(log::LevelFilter::Debug)
        .log_file("/tmp/ydev-test.log")
        .log_runtime(true)
        .log_console(false)
        .build();

    let yv = YoctoVar::new(Some("ABC")).unwrap();
    assert_eq!(yv.name(), "ABC");

    let yv = YoctoVar::new(None).unwrap();
    assert_eq!(yv.name(), "UNDEFINE");
}
