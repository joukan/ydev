#[allow(unused)]
use {
    crate::utils::{scan_dir, scan_dir_for_dir, scan_dir_for_file},
    anyhow::{Context, Error, Result},
    jlogger::{jdebug, jerror, jinfo, jwarn},
    log::{debug, error, info, warn},
    std::fmt::Display,
    std::fs,
    std::path::{Path, PathBuf},
};

pub struct Recipe {
    name: String,
    path: String,
    version: String,
    append: bool,
}

impl Recipe {
    pub fn new(path: &str) -> Result<Self> {
        let mut append = false;
        let mut filename = Path::new(path)
            .file_name()
            .ok_or(Error::msg("Invalid file name."))?
            .to_str()
            .ok_or(Error::msg("Invalid file name."))?;

        match filename {
            _ if filename.ends_with(".bb") => (),
            _ if filename.ends_with(".bbappend") => append = true,
            _ => return Err(Error::msg("Invalid recipe.")),
        }

        let path = path.to_string();

        if append {
            filename = filename.trim_end_matches(".bbappend");
        } else {
            filename = filename.trim_end_matches(".bb");
        }

        let mut version = String::new();
        let mut name = filename.to_string();
        if let Some(i) = filename.rfind("_") {
            version.push_str(format!("{}", &filename[i + 1..]).as_str());
            name = filename[..i].to_string();
        } else {
        }

        Ok(Recipe {
            name,
            path,
            version,
            append,
        })
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn path(&self) -> &str {
        self.path.as_str()
    }

    pub fn version(&self) -> Option<&str> {
        if self.version.is_empty() {
            None
        } else {
            Some(self.version.as_str())
        }
    }

    pub fn is_append(&self) -> bool {
        self.append
    }
}

impl Display for Recipe {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut suffix = ".bb";
        if self.append {
            suffix = ".bbappend";
        }

        if self.version.is_empty() {
            write!(f, "{}{}", self.name, suffix)
        } else {
            write!(f, "{}_{}{}", self.name, self.version, suffix)
        }
    }
}
