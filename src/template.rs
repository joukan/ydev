#[allow(unused)]
use {
    crate::recipe::Recipe,
    crate::utils::{scan_dir, scan_dir_for_dir, scan_dir_for_file, verify_content},
    anyhow::{Context, Error, Result},
    jlogger::{jdebug, jerror, jinfo, jwarn},
    log::{debug, error, info, warn},
    std::collections::HashMap,
    std::fmt::Display,
    std::fs,
    std::path::{Path, PathBuf},
};

pub struct TemplateFile {
    path: PathBuf,
    conf: Option<String>,
    bblayer: Option<String>,
    depends: HashMap<String, ()>,
    content_loaded: bool,
}

impl TemplateFile {
    pub fn new(p: &Path) -> Option<Self> {
        if !p.is_dir() {
            return None;
        }

        Some(TemplateFile {
            path: p.to_path_buf(),
            conf: None,
            bblayer: None,
            depends: HashMap::<String, ()>::new(),
            content_loaded: false,
        })
    }

    pub fn load_content(&mut self) -> Result<()> {
        let mut conf: Option<String> = None;
        let mut bblayer: Option<String> = None;
        let path = &self.path;
        let depends = &mut self.depends;
        if self.content_loaded {
            return Ok(());
        }

        scan_dir_for_file(path.as_path(), &mut |p: PathBuf| -> Result<()> {
            if let Some(l) = p.file_name() {
                if let Some(f) = l.to_str() {
                    if f.ends_with("_local.conf.inc") {
                        let content = fs::read_to_string(p.to_str().unwrap())?;
                        conf = verify_content(content);
                        if conf.is_none() {
                            jwarn!("local config file {} is empty.", p.display());
                        }
                    }

                    if f.ends_with("_bblayers.conf.inc") {
                        let content = fs::read_to_string(p.to_str().unwrap())?;
                        bblayer = verify_content(content);
                        if bblayer.is_none() {
                            jwarn!("bblayer file {} is empty.", p.display());
                        }
                    }

                    if f == "included.dep" {
                        let content = fs::read_to_string(p.to_str().unwrap())?;
                        if content.trim().is_empty() {
                            jwarn!("depends file {} is empty.", p.display());
                        } else {
                            for f in content.split(' ') {
                                let f = f.trim();
                                if !f.is_empty() {
                                    if depends.get(f).is_some() {
                                        jwarn!("Multiple {} found.", f);
                                    }
                                    depends.insert(f.to_string(), ());
                                }
                            }
                        }
                    }
                }
            }
            Ok(())
        })?;

        self.conf = conf;
        self.bblayer = bblayer;
        self.content_loaded = true;
        Ok(())
    }

    pub fn get_filename(&self) -> &str {
        self.path.file_name().unwrap().to_str().unwrap()
    }

    pub fn get_pathname(&self) -> &str {
        self.path.to_str().unwrap()
    }

    pub fn get_conf(&self) -> Option<String> {
        if let Some(c) = &self.conf {
            let mut result = String::new();

            result.push_str(&format!("{}\n", (0..80).map(|_| "#").collect::<String>()));
            result.push_str(&format!("# {}\n", self.get_pathname()));
            result.push_str(c);
            result.push('\n');

            Some(result)
        } else {
            None
        }
    }

    pub fn get_bblayer(&self) -> Option<String> {
        if let Some(c) = &self.bblayer {
            let mut result = String::new();

            result.push_str(&format!("{}\n", (0..80).map(|_| "#").collect::<String>()));
            result.push_str(&format!("# {}\n", self.get_pathname()));
            result.push_str(c);
            result.push('\n');

            Some(result)
        } else {
            None
        }
    }

    pub fn get_depends(&self) -> &HashMap<String, ()> {
        &self.depends
    }
}
