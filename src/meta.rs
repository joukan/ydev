#[allow(unused)]
use {
    crate::recipe::Recipe,
    crate::utils::{scan_dir, scan_dir_for_dir, scan_dir_for_file, verify_content},
    anyhow::{Context, Error, Result},
    jlogger::{jdebug, jerror, jinfo, jwarn},
    log::{debug, error, info, warn},
    std::collections::HashMap,
    std::env,
    std::ffi::OsStr,
    std::fmt::Display,
    std::fs,
    std::path::{Path, PathBuf},
};

pub struct MetaTopDir {
    topdir: String,
}

impl MetaTopDir {
    pub fn new(dir: &str) -> Result<Self> {
        let meta_top = fs::canonicalize(dir)?;
        let mut result = false;

        if meta_top.is_dir() {
            scan_dir(meta_top.as_path(), &mut |dp: PathBuf| -> bool {
                if let Some(a) = dp.file_name() {
                    if a == OsStr::new("poky") {
                        result = true;
                        return false;
                    }
                }
                true
            });
        }

        if !result {
            return Err(Error::msg("Invalid meta top directory."));
        }

        Ok(MetaTopDir {
            topdir: meta_top.to_str().unwrap().to_string(),
        })
    }

    pub fn as_str(&self) -> &str {
        self.topdir.as_str()
    }

    pub fn pathbuf(&self) -> PathBuf {
        Path::new(self.topdir.as_str()).to_path_buf()
    }
}

impl Display for MetaTopDir {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.topdir)
    }
}
