#[allow(unused)]
use {
    anyhow::{Error, Result},
    std::fs,
    std::path::{Path, PathBuf},
};

pub fn scan_dir(p: &Path, cb: &mut dyn FnMut(PathBuf) -> bool) {
    if p.is_dir() {
        if let Ok(entrys) = fs::read_dir(p) {
            for entry in entrys.flatten() {
                if !cb(entry.path()) {
                    break;
                }
            }
        }
    }
}

pub fn scan_dir_for_dir(p: &Path, cb: &mut dyn FnMut(PathBuf) -> bool) {
    if p.is_dir() {
        if let Ok(entrys) = fs::read_dir(p) {
            for entry in entrys.flatten() {
                let p = entry.path();
                if p.is_dir() && !cb(p) {
                    break;
                }
            }
        }
    }
}

pub fn scan_dir_for_file(p: &Path, cb: &mut dyn FnMut(PathBuf) -> Result<()>) -> Result<()> {
    if p.is_dir() {
        if let Ok(entrys) = fs::read_dir(p) {
            for entry in entrys.flatten() {
                let p = entry.path();
                if p.is_file() {
                    cb(p)?;
                }
            }
        }
    }

    Ok(())
}

pub fn verify_content(content: String) -> Option<String> {
    let content = content.trim();
    if !content.is_empty() {
        Some(content.to_string())
    } else {
        None
    }
}
